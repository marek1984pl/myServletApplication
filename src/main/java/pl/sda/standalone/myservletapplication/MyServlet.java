/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.sda.standalone.myservletapplication;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author marecki
 */
public class MyServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {

            String firstName = request.getParameter("first_name");
            String lastName = request.getParameter("last_name");
            
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet MyServlet</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet MyServlet at " + request.getContextPath() + "</h1>");
            out.println("Servlet version : " + getInitParameter("version") + "<br>");
            out.println("Servlet context data : " + getServletContext().getInitParameter("param1") + "<br>");
            out.println("<a href='/MyServletApplication/MyServlet?first_name=Kazik&last_name=Wichura'>Imie i nazwisko</a><br>");
            
            out.println("<a href='/MyServletApplication/page1.html'>Dodatkowa strona w html</a>");
            
            if(request.getParameter("first_name") != null && request.getParameter("last_name") != null)
                out.println("<h3>Nazywasz się " + firstName + " " + lastName + "</h3>");
            
            out.println("<br><br><FORM ACTION='/MyServletApplication/MyServlet' METHOD='POST'>");
            out.println("Twoje imię: <INPUT TYPE='text' name='name_text'/>");
            out.println("<INPUT TYPE='submit' value='Ok'/>");
            out.println("</FORM>");
            
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //processRequest(request, response);
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {

            String firstName = request.getParameter("name_text");

            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet MyServlet</title>");
            out.println("</head>");
            out.println("<body>");

            if (!firstName.isEmpty())
            {
                out.println("<h1>Witaj " + firstName + "!</h1>");
                out.println("<a href='/MyServletApplication/MyServlet'>Powrót</a>");
            }
            else
            {
                out.println("Podaj imię!");
                out.println("<a href='/MyServletApplication/MyServlet'>Powrót</a>");
            }

            out.println("</body>");
            out.println("</html>");
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
